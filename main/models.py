from django.db import models
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse


class UploadCSV(models.Model):
    the_group = models.ForeignKey(Group)
    name = models.CharField(max_length=500)
    item = models.FileField(upload_to='media')
    # firstname = models.CharField(max_length=500)
    # lastname = models.CharField(max_length=500)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        """ For reverse url matching in views """
        return reverse('detail', kwargs={'pk': self.id})
