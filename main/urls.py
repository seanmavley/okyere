from django.conf.urls import patterns, url
from . import views
from django.conf.urls.static import static
from Okyere import settings

urlpatterns = patterns('',
    # url(r'^$', views.homepage, name='homepage'),
    url(r'^$', views.AddCSV, name='homepage'),
    # url(r'^$', views.AddCSV.as_view(), name='homepage'),
    url(r'^detail/(?P<pk>\d+)', views.DetailCSV.as_view(), name='detail'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
