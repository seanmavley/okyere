# Forms.py Handwritten by google.com/+Nkansahrexford
from django.forms import ModelForm
from .models import UploadCSV


class UploadCSVForm(ModelForm):
    class Meta:
        model = UploadCSV
        fields = ('the_group', 'name', 'item')
