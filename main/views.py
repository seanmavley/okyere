from django.shortcuts import render
from django.views.generic import CreateView, DetailView
from .models import UploadCSV
from .forms import UploadCSVForm
import os
import csv

BASE_DIR = os.path.dirname(os.path.dirname(__file__))



def handle_uploaded_file(f):
    with open(BASE_DIR + 'file.csv', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def handle_files(f):
    reader = csv.DictReader(open(f))
    for row in reader:
        id=row['id']
        age=row['age']
        height=row['height']
        my_object = MyObject(id=id, age=age,height=height)
        my_object.save()

# class AddCSV(CreateView):
#     model = UploadCSV
#     template_name = 'index.html'
#     context_object_name = 'homepage'
#     form_class = UploadCSVForm

def AddCSV(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = UploadCSVForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # handle_uploaded_file(request.FILES['item'])
            myfile = request.FILES['item']
            data = [row for row in csv.reader(myfile.read().splitlines())]
            return render(request, 'index.html', {'data': data})


    else:
        form = UploadCSVForm()

    return render(request, 'index.html', {'form': form})


class DetailCSV(DetailView):
    model = UploadCSV
    template_name = 'detail.html'
    context_object_name = 'detail'


def homepage(request):
    return render(request, 'index.html')
