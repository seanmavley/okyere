# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uploadcsv',
            name='firstname',
        ),
        migrations.RemoveField(
            model_name='uploadcsv',
            name='lastname',
        ),
        migrations.AlterField(
            model_name='uploadcsv',
            name='name',
            field=models.FileField(upload_to=b'media'),
        ),
    ]
