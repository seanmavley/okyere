# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150311_1554'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadcsv',
            name='item',
            field=models.FileField(default='/media/item.jpg', upload_to=b'media'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='uploadcsv',
            name='name',
            field=models.CharField(max_length=500),
        ),
    ]
